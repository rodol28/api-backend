const handleErrors = (error, req, res, next) => {
    console.error(error);
    console.log(error.name);

    if (error.name === "CastError") {
        res.status(400).json({ error: error.message });
    } else {
        response.status(500).end()
    }
}

module.exports = handleErrors;