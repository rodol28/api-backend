const { Schema, model } = require("mongoose");
const uniqueValidator = require('mongoose-unique-validator');

const userSchema = new Schema({
    username: { type: String, required: true, unique: true },
    name: String,
    passwordHash: String,
    notes: [
        {
            type: Schema.Types.ObjectId,
            ref: "Note"
        }
    ]
}, {
    toJSON: {
        transform: function (document, returnedObject) {
            returnedObject.id = returnedObject._id
            delete returnedObject._id
            delete returnedObject.__v
            delete returnedObject.passwordHash
        }
    }
});

userSchema.plugin(uniqueValidator);

module.exports = model("User", userSchema);