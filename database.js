const mongoose = require("mongoose");

const { MONGODB_URL, MONGODB_URL_TEST, NODE_ENV } = process.env;

const connectionString = NODE_ENV === "test"
    ? MONGODB_URL_TEST
    : MONGODB_URL;

mongoose.connect(connectionString, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
    useCreateIndex: true
})
    .then(db => {
        if (NODE_ENV !== "test") console.log("DB is connect");
    })
    .catch(err => console.error(err));

process.on("uncaughtException", () => {
    mongoose.connection.close();
});