const request = require("supertest");
const { app } = require("../index");
const User = require("../models/User");

const initialNotes = [
    {
        content: 'HTML is easy',
        date: '2019-05-30T17:30:31.098Z',
        important: true
    },
    {
        content: 'Browser can execute only JavaScript',
        date: '2019-05-30T18:39:34.091Z',
        important: false
    },
];

const getAllNotesFromContent = async () => {
    const response = await request(app).get("/api/notes");
    return {
        contents: response.body.map(note => note.content),
        response
    };
}

const getUsers = async () => {
    const usersDB = await User.find({});
    return usersDB.map(user => user.toJSON());
}

module.exports = { initialNotes, getAllNotesFromContent, getUsers };