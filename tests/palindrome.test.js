const { palindrome } = require("../utils/for_testing");

test.skip("palindrome of sherlock", () => {
    const result = palindrome("sherlock");

    expect(result).toBe("kcolrehs");
});

test.skip("palindrome of empty string", () => {
    const result = palindrome("");

    expect(result).toBe("");
});

test.skip("palindrome of undefined", () => {
    const result = palindrome();

    expect(result).toBeUndefined();
});