const request = require("supertest");
const { app, server } = require("../index");
const Note = require("../models/Note");

const { initialNotes, getAllNotesFromContent } = require("./helpers");

beforeEach(async () => {
    await Note.deleteMany({});

    // Parallel
    // const notesObjects = initialNotes.map(note => new Note(note));
    // const promises = notesObjects.map(note => note.save());
    // await Promise.all(promises);

    // Sequencial
    for (const note of initialNotes) {
        const noteObject = new Note(note);
        await noteObject.save();
    }

}, 30000);

test("notes are returned as json", async () => {
    await request(app)
        .get("/api/notes")
        .expect("Content-Type", /json/)
        .expect(200);
}, 10000);

test("there are two notes", async () => {
    const response = await request(app).get("/api/notes");
    expect(response.body).toHaveLength(initialNotes.length);
}, 10000);

test("the first note is about HTML", async () => {
    const { contents } = await getAllNotesFromContent();
    expect(contents).toContain("HTML is easy");
}, 10000);

test("a valid note can be added", async () => {
    const newNote = {
        content: "Proximamente async/await",
        important: true
    };
    await request(app)
        .post("/api/notes")
        .send(newNote)
        .expect(201)
        .expect("Content-Type", /json/);

    const { contents, response } = await getAllNotesFromContent();

    expect(response.body).toHaveLength(initialNotes.length + 1);
    expect(contents).toContain(newNote.content);

}, 10000);

test("a note without content is not added", async () => {
    const newNote = {
        important: true
    };
    await request(app)
        .post("/api/notes")
        .send(newNote)
        .expect(400);
    const response = await request(app).get("/api/notes");
    expect(response.body).toHaveLength(initialNotes.length);

}, 10000);

test("a note can be deleted", async () => {
    const { response: firstResponse } = await getAllNotesFromContent();
    const { body: notes } = firstResponse;
    const noteToDelete = notes[0];
    await request(app)
        .delete(`/api/notes/${noteToDelete.id}`)
        .expect(204);

    const { response: secondResponse, contents } = await getAllNotesFromContent();
    expect(secondResponse.body).toHaveLength(initialNotes.length - 1);
    expect(contents).not.toContain(noteToDelete.content);
});

test("a note that do not can not be deleted", async () => {
    await request(app)
        .delete(`/api/notes/1234`)
        .expect(400);

    const { response } = await getAllNotesFromContent();
    expect(response.body).toHaveLength(initialNotes.length);
});


afterAll(() => {
    server.close();
});