const bcrypt = require("bcrypt");
const User = require("../models/User");
const request = require("supertest");
const { app, server } = require("../index");
const { getUsers } = require("./helpers");

describe("Creating a new user", () => {
    beforeEach(async () => {
        await User.deleteMany({});

        const passwordHash = await bcrypt.hash("pswd", 10);
        const newUser = new User({ username: "rododev", passwordHash });

        await newUser.save();
    }, 10000);


    test("Works as expected creating a fresh username", async () => {
        const usersAtStart = await getUsers();

        const newUser = {
            username: "sherlock",
            name: "Sherlock Holmes",
            password: "twitch"
        }

        await request(app)
            .post("/api/users")
            .send(newUser)
            .expect(201)
            .expect("Content-Type", /json/);

        const usersAtEnd = await getUsers();

        expect(usersAtEnd).toHaveLength(usersAtStart.length + 1);

        const usernames = usersAtEnd.map(u => u.username);
        expect(usernames).toContain(newUser.username);


    }, 10000);

    // test("Creation fail with proper status code and message if username is already taken", async () => {

    //     const usersAtStart = await getUsers();

    //     const newUser = {
    //         username: "sherlock",
    //         name: "Sherlock Holmes",
    //         password: "testtwitch"
    //     }

    //     const result = await request(app)
    //         .post("/api/users")
    //         .send(newUser)
    //         .expect(400)
    //         .expect("Content-Type", /json/);

    //     expect(result.body.error.errors.username.message).toContain("`username` to be unique");

    //     const usersAtEnd = await getUsers();

    //     expect(usersAtEnd).toHaveLength(usersAtStart.length);

    // }, 10000);


    afterAll(() => {
        server.close();
    });
});