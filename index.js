require("dotenv").config();
const express = require("express");
const cors = require("cors");
const logger = require("./loggerMiddleware");
require("./database");

const handleErrors = require("./middlewares/handleErrors");
const notFound = require("./middlewares/notFound");

const usersRouter = require("./controllers/users");

const Note = require("./models/Note");

const app = express();
app.use(cors());
app.use(express.json());
app.use("/images", express.static("images"));

if (process.env.NODE_ENV !== "test" && process.env.NODE_ENV !== "production") app.use(logger);

app.get("/", (req, res) => {
  res.send("<h1>Hello world</h1>");
});

app.get("/api/notes", async (req, res) => {
  const notes = await Note.find();
  res.status(200).json(notes);
});

app.post("/api/notes", async (req, res) => {
  if (!req.body.content)
    return res.status(400).json({ message: "Note content is missing" });

  const { content, date, important } = req.body;
  const newNote = new Note({ content, date, important });
  try {
    const noteSaved = await newNote.save();
    return res.status(201).json(noteSaved);
  } catch (err) {
    next(err);
  }
});

app.get("/api/notes/:id", async (req, res, next) => {
  const { id } = req.params;
  try {
    const note = await Note.findById(id);
    if (note) {
      return res.json(note);
    }
    else {
      res.status(404).json({ message: "Note not found" });
    }
  } catch (err) {
    next(err);
  }
});

app.delete("/api/notes/:id", async (req, res, next) => {
  const { id } = req.params;
  try {
    const noteDeleted = await Note.findByIdAndDelete(id);
    if (noteDeleted) {
      res.status(204).json(noteDeleted);
    } else {
      res.status(404).json({ message: "Note not found" });
    }
  } catch (err) {
    next(err);
  }
});

app.put("/api/notes/:id", async (req, res, next) => {
  const { id } = req.params;
  const note = req.body;

  const newNoteInfo = {
    content: note.content,
    important: note.important
  };

  try {
    const noteUpdated = await Note.findByIdAndUpdate(id, newNoteInfo, { new: true });
    if (noteUpdated) {
      res.status(200).json(noteUpdated);
    } else {
      res.status(404).json({ message: "Note not found" });
    }
  } catch (err) {
    next(err);
  }
});

app.use("/api/users", usersRouter);

app.use(notFound);
app.use(handleErrors);

const PORT = process.env.PORT || 4000;
const server = app.listen(PORT, () => {
  console.log(`Server running on PORT=${PORT}`);
});

module.exports = { app, server };